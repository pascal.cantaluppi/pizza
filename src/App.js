import React from "react";
import logo from "./pizza.svg";
import "./App.css";

const os = require("os");

//const getId = require("docker-container-id");

/*
async function test() {
  let id = await getId();
  if (!id) {
    console.error("Woah, you need to containerize this thing!");
    process.exit(1);
  } else {
    console.log("I'm in container:", await getId());
  }
}
*/

function App() {
  const hostname = os.hostname();
  //var container = getId();

  //var ip = require("ip");
  //console.dir(ip.address());

  //test();

  return (
    <div className="App">
      <header className="App-header">
        <p>
          <b>Pizza App</b>
        </p>
        <img src={logo} className="App-logo" alt="Pizza" />
        <p>
          <small>
            Node: {hostname}
            {/*<br /> IP: {container}*/}
            {/*<br /> IP: {ip.address()}*/}
          </small>
        </p>
      </header>
    </div>
  );
}

export default App;
